import { defaultConstants } from "./default/defaultConstants";

const state = defaultConstants;

const getters = {}

const actions = {}

const mutations = {}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}