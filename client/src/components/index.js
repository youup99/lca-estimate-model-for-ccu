// Cards

// Forms
import GeneralAssumptionsForm from "./Forms/GeneralAssumptionsForm.vue";
import PathwayForm from "./Forms/PathwayForm.vue";

// Tables
import PathwayCalc from "./Tables/Calculation/PathwayCalc.vue";
import Figure1Table from "./Tables/Figure1Table.vue";

// Charts
import Figure1Chart from "./Charts/figure1/Figure1Chart.vue";
import Figure2Chart from "./Charts/figure2/Figure2Chart.vue";
import Figure3Chart from "./Charts/figure3/Figure3Chart.vue";
import Figure4Chart from "./Charts/figure4/Figure4Chart.vue";

export {
    GeneralAssumptionsForm,
    PathwayForm,
    PathwayCalc,
    Figure1Table,
    Figure1Chart,
    Figure2Chart,
    Figure3Chart,
    Figure4Chart
};